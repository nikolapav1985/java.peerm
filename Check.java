import java.util.Scanner;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.ArrayList;
/**
*
* Check class
*
* it is used to read dictionary and check spelling
*
* ----- compile -----
*
* javac Check.java
*
* ----- run example -----
*
* java Check checkcorrect.txt
*
*/
class Check{
    public static void main(String args[]){
        HashSet<String> aset=new HashSet<String>();
        HashSet<String> bset=new HashSet<String>();
        ArrayList<Integer> cset=new ArrayList<Integer>();
        HashSet<String> dset=new HashSet<String>();
        String filename="words.txt";
        String checkname=""; // a file to check
        String token;
        String tokencheck;
        Scanner filein;
        Scanner filecheck;
        boolean pass;
        try{
            filein = new Scanner(new File(filename));
            for(;filein.hasNext();){ // read all lines in dictionary
                token=filein.next();
                aset.add(token);
            }
            checkname=args[0];
            filecheck=new Scanner(new File(checkname));
            for(;filecheck.hasNext();){
                tokencheck=filecheck.next();
                System.out.println(tokencheck);
                bset.add(tokencheck);
            }
            pass=checkCorrect(aset,bset,cset);
            System.out.println(cset);
            if(!pass){ // look for correct suggestions
                suggest(aset,bset,cset,dset);
            }
            System.out.println(dset);
        }catch(IOException e){
        }
    }
    public static boolean checkCorrect(HashSet<String> aset,HashSet<String> bset,ArrayList<Integer> cset){ // check spelling
        int correct=1; // add result for a line
        int notcorrect=0;
        boolean good=true;
        String token;
        for(Object object : bset){ // iterate each line check set
            token=(String)object;
            if(aset.contains(token)){
                cset.add(correct);
                good=good && true;
            } else {
                cset.add(notcorrect);
                good=good && false;
            }
        }
        return good;
    }
    public static void suggest(HashSet<String> aset,HashSet<String> bset,ArrayList<Integer> cset,HashSet<String> dset){ // add suggestions
        int i=0;
        int len=3;
        String token;
        String tokenb;
        for(Object object : bset){
            token=(String)object;
            if(token.length()<len){ // too short, do not correct
                continue;
            }
            if(cset.get(i)==0){
                for(Object objectb : aset){
                    tokenb=(String)objectb;
                    if(tokenb.length()<len){ // too short, do not correct
                        continue;
                    }
                    if(tokenb.substring(0,2).equals(token.substring(0,2))){ // match initial letters
                        dset.add(tokenb);
                    }
                }
            }
            i++;
        }
    }
}
