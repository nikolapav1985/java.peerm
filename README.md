Assignment 5
------------

- Check.java (a spell checker, check comments for details)

Examples
--------

- java Check checkcorrect.txt (check all correct entries)
- java Check checknotcorrect.txt (check, some entries not correct)

Test environment
----------------

- os lubuntu 16.04 kernel version 4.13.0
- javac version 11.0.5
